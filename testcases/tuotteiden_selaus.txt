Test Case

Test Tuoteryhmien Otsikot
  Avataan Sivu http://automationpractice.com
  Valitaan Tuoteryhmä Dresses
  Varmista että Tuoteryhmä Dresses On Näkyvissä

Prerequisite (Esitilanne)
  Sivu http://automationpractice.com on auki
  Ja ollaan pääsivulla
  firefox
  Ostoskori on tyhjä

Test Lisätään Tuote Ostoskoriin Tuotekuvasta
  Lisätään tuote ostoskoriin suoraan tuotekuvasta Printed Chiffon Dress

Test Lisätään Tuote Ostoskoriin Tuotesivulta
  Etsitään Tuote Printed Chiffon Dress
  Valitaan Tuotteen Tarkemmat Tiedot Printed Chiffon Dress
  Varmista Että Tuotteen Tiedot Ovat Näkyvissä
  Valitaan Tuotteen Määräksi 1
  Valitaan Tuotteen Koko L
  Valitaan Tuotteen Väri Yellow
  Lisätään Tuote Ostoskoriin
  Valitse Ostoskori
  Varmista Että Valitsemasi Tuote On Ostoskorissa Printed Chiffon Dress
  Varmista Että Valitsemasi Tuotteen Tiedot Ovat 1 L Yellow
