*** Settings ***
Library    SeleniumLibrary

Suite Setup    Avataan Sivu     http://automationpractice.com
Suite Teardown    Close Browser

Test Setup   Mene Etusivulle

*** Test Cases ***

Test Tuoteryhmien Otsikot
  Valitaan Tuoteryhmä Dresses
  Varmista että Tuoteryhmä Dresses On Näkyvissä

Test Add Item To Cart
  Valitaan Tuoteryhmä Dresses
  Click Link    xpath=//a[@class='product-name'][contains(.,'Printed Chiffon Dress')]
  Wait Until Page Contains    Printed Chiffon Dress
  Input text    id=quantity_wanted    1
  Select From List By Label    id=group_1    L
  Click Element    id=add_to_cart
  Sleep    1
  Click Element    xpath=//span[contains(@title,'Close window')]
  Click Element    xpath=//a[contains(@title,'View my shopping cart')]
  Wait Until Page Contains Element    id=cart_summary

*** Keywords ***

Avataan Sivu
  [Arguments]    ${urli}
  Open Browser    ${urli}    firefox
  Wait Until Page Contains    Call us now    10s

Valitaan Tuoteryhmä Dresses
  Click Link    xpath=(//a[contains(@title,'Dresses')])[5]
  Wait Until Page Contains Element    id=subcategories

Varmista että Tuoteryhmä Dresses On Näkyvissä
  Wait Until Page Contains    Find your favorites dresses from our wide choice of evening

Mene Etusivulle
    Go to    http://automationpractice.com
    Wait Until Page Contains    Call us now    10s
