*** Settings ***

Library    SeleniumLibrary

*** Test Cases ***

Test Add Item To Cart
    Open Browser    http://automationpractice.com/index.php
    Wait Until Page Contains     Call us now
    Click Link    xpath=//a[@title='Women']
    Wait Until Page Contains    You will find here all woman fashion collections.
    Click Link    xpath=//a[@class='product-name'][contains(.,'Blouse')]
    Wait Until Page Contains    Blouse
    Input text    id=quantity_wanted    2
    Select From List By Label    id=group_1    L
    Click Element    id=add_to_cart
    Sleep    1
    Click Element    xpath=//span[contains(@title,'Close window')]
    Click Element    xpath=//a[contains(@title,'View my shopping cart')]
    Wait Until Page Contains Element    id=cart_summary
    Text Should Contain    xpath=//table[@id=cart_summary]//td[]
