Instructions:
* Installation may want to reboot, reboot after both installations done:
  * Install VirtualBox  www.virtualbox.org
  * Install Vagrant  www.vagrantup.com
* Set the file in attachement (Vagrantfile) in to a empty project folder
  * Open commandline to that folder
  * Execute command vagrant up
    * This will take some time (it downloads 1.7GB image)
* You can login to the virtualbox with vagrant/vagrant
  * Open terminal from start menu -> system tools -> LXterminal
  * Run command pybot test.reboot
    * Should open firefox and run test, after that same test with chrome
